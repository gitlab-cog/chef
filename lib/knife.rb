class Knife
  def initialize()
    chef_url = ENV.fetch('CHEF_URL', 'none')
    chef_node_name = ENV.fetch('CHEF_NODE_NAME', 'none')
    chef_key = ENV.fetch('CHEF_KEY', 'none')

    knife_rb('/tmp/knife.rb', chef_url, chef_node_name)
    pem('/tmp/key.pem', chef_key)
  end

  def status(all)
    if all
      value = `/usr/bin/bundle exec knife status -c /tmp/knife.rb`
    else
      value = `/usr/bin/bundle exec knife status -c /tmp/knife.rb|/usr/bin/grep hours`
      value = 'All nodes are up to date' if value == ""
    end
  end

  def job(command)
    value = `/usr/bin/bundle exec knife job #{command} -c /tmp/knife.rb`
  end

  private
  def knife_rb(file_name, chef_url, chef_node_name)
    f = File.open(file_name, 'w')
    f.write("chef_server_url '#{chef_url}'\n")
    f.write("node_name '#{chef_node_name}'\n")
    f.write("client_key '/tmp/key.pem'\n")
    f.close()
  end

  def pem(file_name, chef_key)
    f = File.open(file_name, 'w')
    f.write(chef_key)
    f.close()
  end
end
