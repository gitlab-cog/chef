require 'cog'
require 'knife'

module CogCmd
  module Chef
    module Chef
      module Job
        class Output < Cog::Command
          def run_command
            jid = request.args[0]
            node = request.args[1]
            raise(Cog::Error, "Missing a job id") if jid.nil? or jid.empty?
            raise(Cog::Error, "Missing a node") if node.nil? or node.empty?
            job_command = "output #{jid} #{node}"
            knife = Knife.new.job(job_command)
            response.content = knife.to_s
          end
        end
      end
    end
  end
end
