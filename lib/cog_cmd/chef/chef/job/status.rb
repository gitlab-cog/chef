require 'cog'
require 'knife'

module CogCmd
  module Chef
    module Chef
      module Job
        class Status < Cog::Command
          def run_command
            jid = request.args[0]
            raise(Cog::Error, "Missing a job id") if jid.nil? or jid.empty?
            job_command = "status #{jid}"
            knife = Knife.new.job(job_command)
            response.content = knife.to_s
          end
        end
      end
    end
  end
end
