require 'cog'
require 'knife'

module CogCmd
  module Chef
    module Chef
      module Job
        class Start < Cog::Command
          def run_command
            command = request.args[0]
            raise(Cog::Error, "Missing a command") if command.nil? or command.empty?
            node = request.args[1]
            search = "-s #{request.options['search']}" if request.options['search']
            raise(Cog::Error, "Missing a node or search string") if (node.nil? or node.empty?) and (search.nil? or search.empty?)

            job_command = "start -b --capture #{command} #{node} #{search}"
            knife = Knife.new.job(job_command)
            response.content = knife.to_s
          end
        end
      end
    end
  end
end
