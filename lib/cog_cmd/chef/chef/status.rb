require 'cog'
require 'knife'

module CogCmd
  module Chef
    module Chef
      class Status < Cog::Command
        def run_command
          knife = Knife.new.status(!!request.options['all'])
          response.content = knife.to_s
        end
      end
    end
  end
end
