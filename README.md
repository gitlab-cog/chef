# No Further Cog Bundle Development

Since cog is [no longer maintained](https://github.com/operable/cog), we will be 
deprecating it over time and instead developing [GitLab ChatOps](https://gitlab.com/gitlab-org/gitlab-ce/issues/34311).
For now, this means that no new features should be added to this repo (or the 
other gitlab-cog group repos). Existing features can continue to be used until 
the new solution is in place. Contributions to GitLab ChatOps are welcomed.
